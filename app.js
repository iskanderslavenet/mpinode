var express = require('express');

var dbConfig = require('./db');
var mongoose = require('mongoose');

//коннектимся к базе
mongoose.connect(dbConfig.user, { useNewUrlParser: true });

var app = express();

//подключаем passport
var passport = require('passport');

//конфигурируем сессию
//app.use(express.cookieParser());
var expressSession = require('express-session');
var MongoStore = require('connect-mongo')(expressSession);

app.use(expressSession(
    {
        secret: 'mySecretKey',
        resave: false,
        saveUninitialized: false,
        store: new MongoStore({
            host: '127.0.0.1',
            port: '27017',
            db: 'sessions',
            url: 'mongodb://127.0.0.1:27017/sessions'
        })
    }
));

app.use(passport.initialize());
app.use(passport.session());

var flash = require('connect-flash');
app.use(flash());

//инициализируем passport
var initPassport = require('./passport/passport-config');
initPassport(passport);

//подключаем роутинг
var routes = require('./routes/index')(passport);
app.use('/', routes);

app.engine('html', require('ejs').renderFile);
app.set('view engine', 'html');

app.use(function(req, res, next) {
    res.header("Access-Control-Allow-Origin", "*"); // update to match the domain you will make the request from
    res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
    next();
  });
  

app.listen(3000);
