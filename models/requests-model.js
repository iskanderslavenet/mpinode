var mongoose = require('mongoose');
 
module.exports = mongoose.model('requests',{
    requestId: Number,
    firstName: String,
    lastName: String,
    surName: String,
    passport: String,
    time: String,
    summ: Number,
    email: String,
    phone: String,
    status: String,
    comment: String,
    adminComment: String,
    collector: String,
});
