var mongoose = require('mongoose');
 
module.exports = mongoose.model('usermpi',{
    firstName: String,
    lastName: String,
    surname: String,
    passport: String,
    phone: String,
    email: String,
});
