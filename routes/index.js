var express = require('express');
var router = express.Router();
fs = require('fs');
var User = require('../models/user-mpi-model');
var Requests = require('../models/requests-model');

var isAuthenticated = function (req, res, next) {
  // if user is authenticated in the session, call the next() to call the next request handler 
  // Passport adds this method to request object. A middleware is allowed to add properties to
  // request and response objects
  if (req.isAuthenticated())
    return next();
  // if the user is not authenticated then redirect him to the login page
  res.setHeader("Content-type", "Application/json");
  res.json({ status: "not logged in" })
}

var isValidUser = function (req, res, next) {
  console.log("IS USER VALID");
  //var musname = req.query.musician_name;
  var musname = req.query.username;
  var passwd = req.query.password;
  User.findOne({ username: musname }, (err, resp) => {
    if (err)
      console.log(err);
    console.log(resp);
    if (resp == null) {
      console.log('USER NOT FOUND');
      res.send("USER NOT FOUND");
      return;
    }

    if (passwd != resp.password) {
      console.log('ACCESS ERROR');
      res.send("ACCESS ERROR'");
      return;
    }
    else {
      console.log(`USER ${musname} VALID`);
      return next();
    }
  })
}

module.exports = function (passport) {

  router.get('/passportok', (req, res) => {
    res.setHeader('Content-type', 'application/json');
    res.json({ status: "ok" });
  });

  router.get('/passportfail', (req, res) => {
    res.setHeader('Content-type', 'application/json');
    res.json({ status: "fail" });
  });

  /* Handle Login POST */
  router.post('/login', passport.authenticate('login', {
    successRedirect: '/passportok',
    failureRedirect: '/passportfail',
    failureFlash: true
  }));

  /* Handle Registration POST */
  router.post('/signup', passport.authenticate('signup', {
    successRedirect: '/passportok',
    failureRedirect: '/passportfail',
    failureFlash: true
  }));

  router.post('/signout', (req, res) => {
    req.logout();
    //res.redirect('/login');
    res.setHeader("Content-type", "Application/json");
    res.send({ status: "ok" });
  });

  router.post('/getaccount', isAuthenticated, isValidUser, (req, res) => {
    res.setHeader("Content-type", "Application/json");
    User.findOne({ username: req.query.username }, (err, doc) => {
      if (err)
        res.send(err)
      res.send(doc);
    })
  })

  router.post('/getdetails', (req, res) => {
    Requests.findOne({requestId: req.query.requestId}, (err, doc) => {
      if(err)
        console.log(err);
      console.log(doc);
      res.setHeader("content-type", "application/json");
      res.send(doc);
    });
  })

  router.post('/leavecomment', (req, res) => {
    Requests.findOneAndUpdate({ requestId: req.query.requestId }, { $set:{adminComment: req.query.comment}}, (err, doc) => {
      if(err)
        console.log(err)
      console.log(doc);
      res.setHeader("content-type", "application/json");
      res.send({status: "ok"});
    })
  })

  router.get('/getrequests', (req, res) => {
    res.render(`${__dirname}\\pages\\requests\\requests.html`);
  })

  router.post('/getrequests', (req, res) => {
    var moment = require('moment');
    Requests.find({}, (err, doc) => {
      if (err)
        res.send(err);
      doc.forEach((val) => {
        var date = moment(val.time).format('DD.MM.YYYY');
        val.time = date;
      })
      res.setHeader("Content-type", "Application/json");
      console.log(doc);
      res.send(doc);
      console.log('----------------------------------------------END------------------------------------');
    })
  });

  router.post('/getusers', isAuthenticated, (req, res) => {
    User.find({}, (err, doc) => {
      if (err)
        res.send(err);
      res.setHeader("Content-type", "Application/json");
      console.log(doc);
      res.send(doc);
    })
  });

  router.get('/calculate', (req, res) => {
    res.render(`${__dirname}\\pages\\requests\\calculate.html`);
  })

  router.post('/calculate', (req, res) => {
    //данные передаём пока что через урлу, если нужно перепишу под передачу через тело запроса
    var percent = 10;
    var moment = require('moment');
    moment.locale('ru');
    res.setHeader("Content-type", "Application/json");
    var summ = parseInt(req.query.summ); //В урле summ - сумма займа
    var nsumm = parseInt(req.query.summ);
    var time = parseInt(req.query.time); //В урле time - время выплаты займа
    summ = (summ / 100 * percent) * time;
    moment = moment().add(time, 'days');
    res.setHeader("Access-Control-Allow-Origin", "*"); // update to match the domain you will make the request from
    res.setHeader("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
    res.send({ summ: Math.ceil(summ + nsumm), day: moment.format('DD.MM.YYYY') })
  });

  router.post('/requestmoney', (req, res) => {
    let newReuquest = new Requests();
    var moment = require('moment');
    moment.locale('ru');
    moment = moment().add(parseInt(req.query.time), 'days');
    console.log(moment.format('LL'));
    Requests.find({}, (err, doc) => {
      if(err)
        console.log(err)
      var requestId = doc[doc.length - 1].requestId;
      newReuquest.firstName = req.query.firstName;
      newReuquest.lastName = req.query.lastName;
      newReuquest.surName = req.query.surName;
      newReuquest.passport = req.query.passport;
      newReuquest.email = req.query.email;
      newReuquest.comment = req.query.comment;
      newReuquest.summ = req.query.summ;
      newReuquest.time = moment;
      newReuquest.status = "expectation";
      newReuquest.adminComment = "";
      newReuquest.requestId = requestId + 1;
      newReuquest.save();
      res.send({ status: "ok" });
    })
  })

  router.post('/grabdebt', isAuthenticated, (req, res) => {
    Requests.findOneAndUpdate({ requestId: req.query.requestId }, { $set: { collector: req.query.collectorName} }, (err, doc) => {
      if (err)
        console.log(err);
      console.log(doc);
      res.setHeader("content-type", "application/json");
      res.send({status: "ok"});
    })
  })

  router.get('/debts', (req, res) => {
    res.render(`${__dirname}\\pages\\requests\\debt.html`);
  })

  router.post('/debts', (req, res) => {
    var now = require('moment');
    var moment = require('moment');
    var percent = 1;
    var debtArray = [];
    var returnVal = {};
    console.log(req);
    now().format('LL');
    Requests.find({}, (err, doc) => {
      if (err)
        res.send(err);
      res.setHeader("Content-type", "Application/json");
      doc.forEach((val) => {
        var debt = moment(val.time).format('LL');
        returnVal = val;
        if (!now().isBefore(debt)) {
          returnVal.difference = val.summ / 100 * percent * now().diff(debt, 'days'); //сумма денег, которую получит коллектор из рассчёта 1% за день просрочки
          console.log(returnVal.difference);
          console.log(val.collector);
          debtArray.push({ collector:val.collctor, requestId: val.requestId, surName: val.surName, firstName: val.firstName, lastName: val.lastName, debt_summ: returnVal.difference, time: moment(val.time).format('DD MM YYYY'), summ: val.summ, debt_time: now().diff(debt, 'days')});
        }
      })
      res.send(debtArray);
    })
  })

  router.get('/getuserinfo', (req, res) => {

  })

  router.post('/getuserinfo', (req, res) => {
    User.findOne({ name: req.query.username }, (err, doc) => {
      if (err)
        console.log(err);
      res.setHeader("content-type", "application/json");
      res.send(doc);
    })
  });

  router.post('/approve', (req, res) => {
    console.log('approving...');
    Requests.findOneAndUpdate({ requestId: req.query.requestId }, { $set: { status: "approved" } }, (err, doc) => {
      if (err)
        console.log(err);
      console.log(doc);
      res.setHeader("content-type", "application/json");
      res.send({status: "ok"});
    })
  })

  return router;
}