//главный узел, отправляет запросы к микросервисам от клиентского приложения
const cote = require('cote');

const MongoClient = require('mongodb').MongoClient;

const requesterProfile = new cote.Requester({ name: 'get info requester', key: 'acc' });
const requesterTrack = new cote.Requester({ name: 'load track requester', key: 'trck' });

const exp = require('express');
const app = exp();

const session = require('express-session');

const passport = require('passport');
const LocalStrategy = require('passport-local').Strategy;

//сессия
app.use(session({secret: 'mySecretKey'}));
app.use(passport.initialize());
app.use(passport.session());

//серилизация/десирилизация пользователя, чтобы каждый раз не отсылать полный набор пользовательских данных для сессии(мандат)
passport.serializeUser(function(user, done) {
    done(null, user._id);
  });
   
  passport.deserializeUser(function(id, done) {
    User.findById(id, function(err, user) {
      done(err, user);
    });
  });

var isValidPassowrd = function(user, password){
    return bCrypt.compareSync(password, user.password);
}

function authenticationMiddleware () {
    return function (req, res, next) {
        if (req.isAuthenticated()) {
            return next()
        }
        console.log('fff');
        res.redirect('/')
    }
}

//Стратегия авторизации, вызывается при авторизации пользователя
passport.use('login', new LocalStrategy({passReqToCallback: true}, (req, username, password, done) => {
    const mongoClient = new MongoClient("mongodb://localhost:27017", { useNewUrlParser: true });
    mongoClient.connect((err, client) => {
        if (err) {
            resp('[ADD USER] Database Error: ' + err);
            return;
        }
        const collection = client.db("userspace").collection("users");
        collection.findOne({'username' : username}, (err, user) =>{
            if(err){
                return done(err);
            }
            if(!user){
                console.log('[AUTENTIFICATION]: user not found');
                return done(null, false);
            }
            if(!isValidPassowrd(user, password)){
                console.log('[AUTENTIFICATION]: invalid password');
            }
            return done(null, user);
        });
    });
}));

//Стратегия создания нового пользователя, вызывается при создании нового пользователя
passport.use('signup', new LocalStrategy({ passReqToCallback: true }, (req, username, password, email, role, done) => {
    done(requesterProfile.send({ type: 'signup', 'login': username, 'password': password, 'email': email, 'role': role }));
}));

app.get('/account', (req, resp) => {
    switch(req.query.reqtype){
        case 'add user':
            requesterProfile.send({ type: 'add user', login: 'login', password: 'password', email: 'email', role: "sounddesigner" }, (res) => {
            console.log(res);
            resp.send(res);
        });
        break;

        case 'get profile':
            authenticationMiddleware();
            /*requesterProfile.send({ type: 'get profile', login: 'login', password: 'password', flogin: 'admin' }, (res) => {
            console.log(res);
            resp.send(res);*/
        break;

        case 'change profile':
        requesterProfile.send({ type: 'change profile', login: 'login', password: 'password', flogin: 'admin', change: "login", to: "new login" }, (res) => {
            console.log(res);
            resp.send(res);
        });
        break;
    }
});

app.post('/track', (req, resp) => {
    console.log('[TRACK]' + req.headers);
    resp.send('Track');
})

app.listen(3000);

///requesterTrack.send();